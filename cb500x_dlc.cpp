#include "cb500x_dlc.h"

uint16_t gear_ratio[NUMBER_OF_GEARS + 1] = { 160, 155, 100,  75,  61,  54,  49 };

// Oled
#ifdef USE_0_96_OLED
//U8GLIB_SSD1306_128X64 oled(U8G_I2C_OPT_DEV_0 | U8G_I2C_OPT_NO_ACK | U8G_I2C_OPT_FAST); // Fast I2C / TWI
#elif defined USE_1_3_OLED
U8GLIB_SH1106_128X64 oled(U8G_I2C_OPT_DEV_0 | U8G_I2C_OPT_FAST); // Dev 0, Fast I2C / TWI
#endif

void init_cb500x_dlc(void) {
  init_display();

  while (dlc_init()) {
    delay(1000);
  }
  delay(100);
  clear_lcd();
}

void init_display(void) {

  oled.setColorIndex(1);
  oled.setFont(u8g_font_6x12);

  // Draw honda logo
  oled.firstPage();
  do {
    oled.drawBitmapP(40, 13, 6, 38, honda_logo);
  } while ( oled.nextPage() );
}

void clear_lcd(void) {
  do {
  } while ( oled.nextPage() );
}

uint8_t calculate_gear(uint16_t ratio) {

  //// If Ratio below first gear, simply return 0
  //if (ratio > gear_ratio[0]) return('-');

  // loop through actual gear ratios and bail if we find out slot
  for (int i = 1; i < NUMBER_OF_GEARS; i++) {
    if (ratio > ((gear_ratio[i] + gear_ratio[i + 1]) / 2)) return (i);
  }

  // Return Top gear if we make it this far (No upper boundary set)
  return (NUMBER_OF_GEARS);
}

void show_info_lcd(int data[]) {
  //Serial.println("show info: ");
  oled.firstPage();
  do {
    /*********************************************
      Calculate current gear based on RPM and VSS
    *********************************************/
    uint16_t ratio_rpm_speed = 0;
    uint8_t current_gear[2] = "-";
    if (data[VSS] != 0 && data[NEU_C] == IN_GEAR) {
      ratio_rpm_speed = data[RPM] / data[VSS];
      current_gear[0] = calculate_gear(ratio_rpm_speed) + 0x30; // Convert int to ascii char

      // If the speed is 0 but we're in gear, assume is 1st gear
      // Note, the bike usually takes some time to read the current speed when starting to move
    } else if (data[VSS] == 0 && data[NEU_C] == IN_GEAR) {
      current_gear[0] = '1';
    } else if (data[NEU_C] == NEUTRAL_OR_CLUTCH) {
      current_gear[0] = '-';
    } else if (data[NEU_C] == KICKSTAND) {
      current_gear[0] = '/';
    }
    //    Serial.print("rpm = ");
    //    Serial.print(data[RPM]);
    //    Serial.print(" | vss = ");
    //    Serial.print(data[VSS]);
    //    Serial.print(" | ratio= ");
    //    Serial.print(ratio_rpm_speed = data[RPM] / data[VSS]);
    //    Serial.print(" | NEU_C= ");
    //    Serial.print(data[NEU_C]);
    //    Serial.print(" | gear= ");
    //    Serial.println(String(current_gear[0], DEC));

    /**********************
       Print current gear
    **********************/
    oled.setFont(u8g_font_fub49n);  // W: 39, H: 49
    oled.setFontRefHeightText();    // Measure the font from the bottom
    oled.setFontPosCenter();        // Use the Center of the font for the positioning

    char gear_font_width = oled.getStrWidth("4");
    //char gear_font_height = oled.getFontAscent();

    oled.drawStr(2, oled.getHeight() / 2, current_gear);
    oled.drawFrame(0, 0, gear_font_width + 2, oled.getHeight());

    /*************************
       Print separation lines
    *************************/
    oled.drawHLine(gear_font_width + 2, (oled.getHeight() / 2) + 1, oled.getWidth() - gear_font_width + 2); // Horizontal line to separate temperatures

    /**********************
       Print temperatures
    **********************/
    oled.setFont(u8g_font_profont29r); // W: 16, H: 19
    oled.setFontPosTop();     // Use the Top of the font for the positioning
    oled.setFontRefHeightText();
    char temp_font_width = oled.getStrWidth("4");
    char temp_font_height = oled.getFontAscent();

    char temp_icon_width = 11;
    char temp_icon_pos_x = gear_font_width + 10;

    char temp_iat_pos_x = temp_icon_pos_x + temp_icon_width + 5;
    char temp_ect_pos_x = temp_iat_pos_x;
    char temp_iat_pos_y = ((oled.getHeight() - 1) / 2) - temp_font_height;
    char temp_ect_pos_y = (oled.getHeight() - 1) - temp_font_height;

    // Print temperature icons
    oled.drawBitmapP(temp_icon_pos_x, temp_iat_pos_y + 1, 2, 19, temp_logo );
    oled.drawBitmapP(temp_icon_pos_x, temp_ect_pos_y + 1, 2, 19, temp_logo );

    char str[4];
    // Intake Air Temperature (IAT)
    sprintf(str, "%3d", data[IAT_T]); // 3 digits
    oled.drawStr(temp_iat_pos_x, temp_iat_pos_y, str);

    // Engine Coolant Temperature (ECT)
    sprintf(str, "%3d", data[ECT_T]); // 3 digits
    oled.drawStr(temp_ect_pos_x, temp_ect_pos_y, str);

    oled.setFont(u8g_font_6x12); // W: 6, H: 9
    oled.setFontPosTop();     // Use the Top of the font for the positioning
    oled.setFontRefHeightText();
    oled.drawStr(temp_iat_pos_x, 1, "Intake");
    oled.drawStr(temp_iat_pos_x, 34, "Engine");

    // Print temperature units
    oled.setFont(u8g_font_6x12);
    oled.setFontPosTop();     // Use the Top of the font for the positioning
    oled.setFontRefHeightText();
    oled.drawStr((sizeof(str) / sizeof(*str) - 1)*temp_font_width + temp_ect_pos_x, temp_iat_pos_y + 12, "\260C");
    oled.drawStr((sizeof(str) / sizeof(*str) - 1)*temp_font_width + temp_iat_pos_x, temp_ect_pos_y + 12, "\260C");

  } while ( oled.nextPage() );
}

void read_table_honda(void) {
  /*
    This is only for getting the table withous specifying the offset
    Table 0x00 - Size : 0x0F
    Table 0x11 - Size : 0x19
    Table 0x20 - Size : 0x08
    Table 0xD0 - Size : 0x1A
    Table 0xD1 - Size : 0x0B
  */
  byte table_0x11[0x1A] = {0}; // 0x14 bytes data + 6 bytes header + crc
  byte table_0xD1[0x0C] = {0}; // 0x06 bytes data + 6 bytes header + crc

  // Read tables from bike
  if (!dlc_get_table_offset(0x11, 0x00, 0x14, table_0x11)) {
    delay(10);
    if (!dlc_get_table_offset(0xD1, 0x00, 0x06, table_0xD1)) {
      int data[13] = {
        table_0x11[5] * 256 + table_0x11[6],      // [0]  RPM
        table_0x11[7],                            // [1]  TPS Voltage (* 5/256)
        table_0x11[8],                            // [2]  TPS % (/ 1.6)
        table_0x11[9],                            // [3]  ECT Voltage (* 5/256)
        table_0x11[10] - 40,                      // [4]  ECT deg C (- 40)
        table_0x11[11],                           // [5]  IAT Voltage (* 5/256)
        table_0x11[12] - 40,                      // [6]  IAT deg C (- 40)
        table_0x11[13],                           // [7]  MAP Voltage (* 5/256)
        table_0x11[14],                           // [8]  MAP kpa
        table_0x11[17],                           // [9]  Battery Voltage (/10)
        table_0x11[18],                           // [10] VSS in Km/h
        table_0x11[19] * 256 + table_0x11[20],    // [11] Fuel injection factor
        table_0xD1[5]                             // [12] (1)Neut-Clutch/(0)Gear/(3)Kickstand
      };

      show_info_lcd(data);

    } else {
      dlc_init();
      delay(500);
    }

    delay(250);

  } else {
    dlc_init();
    delay(500);
  }
}
