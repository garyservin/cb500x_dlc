#include "cb500x_dlc.h"
#include <avr/wdt.h>

void setup()
{
  //Serial.begin(9600); // For debugging
  //watchdogSetup();
  init_cb500x_dlc();
}

void loop() {
  read_table_honda();
}

void watchdogSetup(void)
{
  cli();
  wdt_reset();
  /*
    WDTCSR configuration:
    WDIE = 1: Interrupt Enable
    WDE = 1 :Reset Enable
    See table for time-out variations:
    WDP3 = 0 :For 1000ms Time-out
    WDP2 = 1 :For 1000ms Time-out
    WDP1 = 1 :For 1000ms Time-out
    WDP0 = 0 :For 1000ms Time-out
  */
  // Enter Watchdog Configuration mode:
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  // Set Watchdog settings:
  WDTCSR = (1 << WDIE) | (1 << WDE) |
           (0 << WDP3) | (1 << WDP2) | (1 << WDP1) |
           (0 << WDP0);

  sei();
}

ISR(WDT_vect)
{
  // Include your interrupt code here.
}

