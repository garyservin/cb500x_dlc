#ifndef DLC_H
#define DLC_H

#include <Arduino.h>
#include <SoftwareSerial.h>

#define arr_len( x )  ( sizeof( x ) / sizeof( *x ) )
#define BIKE_REPLY_CMD 0x02

//#define USE_SHIELD_TEST
#ifdef USE_SHIELD_TEST
#define DLC_RX  11
#define DLC_TX  10
#else
#define DLC_RX  13
#define DLC_TX  12
#endif

extern SoftwareSerial dlcSerial;

byte calculate_crc(byte data[], int data_size);

void dlc_send_data(byte data[], int data_size);

int dlc_init(void);

// The command 72,05,71,Table#,chksum requests all the parameters of the specified Table
int dlc_get_table(byte table, byte response[]);

/* The command 72,07,72,Table#,Offset,numBytes,chksum requests
   a subset of parameters from the specified table sending numBytes
   of bytes starting at Offset.
*/
int dlc_get_table_offset(byte table, byte offset, byte len, byte response[]);

#endif
