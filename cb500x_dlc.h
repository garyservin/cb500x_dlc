#ifndef CB500X_DLC_H
#define CB500X_DLC_H

#include "dlc.h"

#include <U8glib.h>
#include <Wire.h>

#define USE_1_3_OLED

#include "honda_logo.h"

#define NUMBER_OF_GEARS 6
extern uint16_t GearRatio[NUMBER_OF_GEARS + 1];

#define RPM     0
#define TPS_V   1
#define TPS_P   2
#define ECT_V   3
#define ECT_T   4
#define IAT_V   5
#define IAT_T   6
#define MAP_V   7
#define MAP_K   8
#define BAT_V   9
#define VSS     10
#define FIF     11
#define NEU_C   12

#define IN_GEAR             0
#define NEUTRAL_OR_CLUTCH   1
#define KICKSTAND           3

// Oled
#ifdef USE_0_96_OLED
extern U8GLIB_SSD1306_128X64 oled;
#elif defined USE_1_3_OLED
extern U8GLIB_SH1106_128X64 oled;
#endif

void read_table_honda(void);
void init_cb500x_dlc(void);
void set_pixel(byte pixel_status);
void init_display(void);
void clear_lcd(void);
uint8_t CalculateGear(uint16_t ratio);
void show_info_lcd(int data[]);
#endif
