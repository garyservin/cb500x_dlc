#include "dlc.h"

//#define DEBUG
//#define DEBUG_SEND

SoftwareSerial dlcSerial(DLC_RX, DLC_TX);// rx_pin, tx_pin

byte calculate_crc(byte data[], int data_size) {
  byte crc = 0;

  for (int i = 0; i < data_size; i++) {
    crc -= data[i];
  }
  return crc;
}

void check_and_read_pending(byte chat) {
  int available_to_read = dlcSerial.available();
  byte tmp[available_to_read] = {};
  if (available_to_read) {
    dlcSerial.readBytes(tmp, available_to_read);
  }

#ifdef DEBUG_VERBOSE
  // Print available chars to read
  Serial.print(char);
  Serial.print(available_to_read, HEX);
  Serial.println(char);
#endif

}
void dlc_send_data(byte data[], int data_size) {

  check_and_read_pending("_");

#ifdef DEBUG_SEND
  Serial.print("Send: ");
#endif
  for (int i = 0; i < data_size; i++) {
    dlcSerial.write(data[i]);
#ifdef DEBUG_SEND
    Serial.print("0x");
    Serial.print(data[i], HEX);
    Serial.print(", ");
#endif
  }
#ifdef DEBUG_SEND
  Serial.println();
#endif
}

int dlc_receive_data(byte data[], int data_size) {

  check_and_read_pending("-");

  int recv_bytes = dlcSerial.readBytes(data, data_size);

#ifdef DEBUG_VERBOSE
  Serial.print('<');
  Serial.print(data_size, HEX);
  Serial.print(',');
  Serial.print(recv_bytes, HEX);
  Serial.print(',');
  Serial.print(dlcSerial.available(), HEX);
  Serial.print(" | ");
  for (int i = 0; i < recv_bytes; i++) {
    Serial.print(data[i], HEX);
    Serial.print(',');
  }
  Serial.println('>');
#endif

  if (recv_bytes != data_size || data[0] != BIKE_REPLY_CMD) {
#ifdef DEBUG
    Serial.println("Failed to read data");
#endif
    return 1;
  }

#ifdef DEBUG
  Serial.print("Recv: ");
  for (int i = 0; i < data_size; i++) {
    Serial.print("0x");
    Serial.print(data[i], HEX);
    Serial.print(", ");
  }
  Serial.println();
#endif

  // checksum
  byte crc = calculate_crc(data, data_size - 1);
  if (crc != data[data_size - 1]) { // checksum failed
    return 1; // data error
  }
  return 0; // success
}

int dlc_init(void) {
  // 300 ms high
  digitalWrite(DLC_TX, HIGH);
  delay(300);

  // 70 ms low
  digitalWrite(DLC_TX, LOW);
  delay(70);

  // 120ms high
  digitalWrite(DLC_TX, HIGH);
  delay(120);

  // Baud 10400
  dlcSerial.begin(10400);

  // FE 04 FF + checksum (FF)
  byte init[4] = {0xFE, 0x04, 0xFF, 0xFF};
  dlc_send_data(init, 4);

  delay(20);

  // 72 05 00 F0 + checksum (99)
  byte request[5] = {0x72, 0x05, 0x00, 0xF0, 0x99};
  dlc_send_data(request, 5);

  // Receive -> 02 04 00 FA
  byte data[4];
  return dlc_receive_data(data, 4);
}

int dlc_get_table(byte table, byte response[]) {
  byte request[] = {0x72, 0x05, 0x71, table, 0x00};
  int request_size = arr_len(request);

  request[request_size - 1] = calculate_crc(request, request_size);

  dlc_send_data(request, request_size);

  byte header_size = 2;
  byte init_data[header_size];
  if (dlcSerial.readBytes(init_data, header_size) != header_size) {
    return 1;
  }

#ifdef DEBUG
  Serial.print("Recv: ");
  for (int i = 0; i < header_size; i++) {
    Serial.print("0x");
    Serial.print(init_data[i], HEX);
    Serial.print(", ");
  }
#endif

  int bytes_to_read = init_data[1] - header_size;
  byte data_tmp[bytes_to_read];
  if (dlcSerial.readBytes(data_tmp, bytes_to_read) != bytes_to_read) {
    return 1;
  }

#ifdef DEBUG
  for (int i = 0; i < bytes_to_read; i++) {
    Serial.print("0x");
    Serial.print(data_tmp[i], HEX);
    Serial.print(", ");
  }
  Serial.println();
#endif

  for (int i = header_size; i < init_data[1]; i++) {
    response[i] = data_tmp[i - header_size];
  }
  // checksum
  byte crc = calculate_crc(response, 6);
  if (crc != response[response[1] - 1]) { // checksum failed
    return 1; // data error
  }
  return 0; // success
}

int dlc_get_table_offset(byte table, byte offset, byte len, byte response[]) {
  byte request[] = {0x72, 0x07, 0x72, table, offset, len, 0x00};
  int request_size = arr_len(request);
  int resp_len = len + 6; // To account for header + crc

  request[request_size - 1] = calculate_crc(request, request_size);

  dlc_send_data(request, request_size);

  return dlc_receive_data(response, resp_len);
}
